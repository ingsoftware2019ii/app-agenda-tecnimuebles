import { Injectable } from '@angular/core';
import { GLOBAL } from '../global';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';//npm install --save rxjs-compat
import 'rxjs/add/operator/map'

@Injectable({
  providedIn: 'root'
})
export class ContratoService {
  public url="";
  public token="";

  constructor(public http:Http) {
      this.url=GLOBAL.url;
      this.token='bearer '+localStorage.getItem('token');
  }

  listarContratoss(id_contrato){
    const headers=new Headers({
        'Content-Type':'application/json',
        'Authorization':this.token
    });
    return this.http.get(this.url+'contrato/list/'+id_contrato,{headers:headers}).map(res=>res.json());
  }
}
