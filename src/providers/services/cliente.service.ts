import { Injectable } from '@angular/core';
import { GLOBAL } from '../global';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';//npm install --save rxjs-compat
import 'rxjs/add/operator/map'

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  public url="";
  public token="";

  constructor(public http:Http) {
      this.url=GLOBAL.url;
      this.token='bearer '+localStorage.getItem('token');
  }

  listarClientes(id_cliente){
    const headers=new Headers({
        'Content-Type':'application/json',
        'Authorization':this.token
    });
    return this.http.get(this.url+'cliente/list/'+id_cliente,{headers:headers}).map(res=>res.json());
  }

}
