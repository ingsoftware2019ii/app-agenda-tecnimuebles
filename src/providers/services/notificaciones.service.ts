import { Injectable } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { LocalNotifications, ELocalNotificationTriggerUnit } from '@ionic-native/local-notifications/ngx';

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  scheduled = [];
  constructor(
    private plt: Platform,
    private localNotifications: LocalNotifications,
    private alertCtrl: AlertController
  ) { 
    this.plt.ready().then(()=>{
      this.localNotifications.on('click').subscribe(res => {
        console.log('click', res);
        let msg = res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg)
      })
    })

    this.plt.ready().then(()=>{
      this.localNotifications.on('trigger').subscribe(res => {
        console.log('trigger', res);
        let msg = res.data ? res.data.mydata : '';
        this.showAlert(res.title, res.text, msg)
      })
    })
  }

  scheduleNotification(data){
    this.localNotifications.schedule({
      id: data.id,
      title: 'App Tecnimuebles',
      text: data.titulo,
      data: {mydata: data.descripcion},
      //trigger: {in: 5, unit: ELocalNotificationTriggerUnit.SECOND},
      trigger: {at: data.fecha},
      //trigger: {at: new Date(new Date().getTime() + 5 * 1000)},
      //trigger: {every: ELocalNotificationTriggerUnit.MINUTE},
      //trigger: {every: { hour: 11, minute: 45}}
      foreground: true //Se puede ejecutar en segundo plano
    })
  }

  showAlert(header, sub, msg){
    this.alertCtrl.create({
      header: header,
      subHeader: sub,
      message: msg,
      buttons: ['Ok']
    }).then(alert => alert.present());
  }

  getAll(){
    this.localNotifications.getAll().then(res=>{
      this.scheduled = res;
    })
  }
}
