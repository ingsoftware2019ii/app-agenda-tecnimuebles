import { Injectable } from '@angular/core';
import { GLOBAL } from '../global';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';//npm install --save rxjs-compat
import 'rxjs/add/operator/map'

@Injectable({
  providedIn: 'root'
})
export class ActividadService {
  public url="";
  public token="";

  constructor(public http:Http) {
      this.url=GLOBAL.url;
      this.token='bearer '+localStorage.getItem('token');
  }

  listarActividades(){
    const headers=new Headers({
        'Content-Type':'application/json',
        'Authorization':this.token
    });
    return this.http.get(this.url+'actividad/list',{headers:headers}).map(res=>res.json());
  }

  listarUltimasActividades(){
    const headers=new Headers({
        'Content-Type':'application/json',
        'Authorization':this.token
    });
    return this.http.get(this.url+'actividad/ultimos',{headers:headers}).map(res=>res.json());
  }

  registrarActividad(actividad){
    const headers=new Headers({
      'Content-Type':'application/json',
      'Authorization':this.token
    });
    return this.http.post(this.url+'actividad/registrar', actividad,{headers:headers}).map(res=>res.json());
  }
}
