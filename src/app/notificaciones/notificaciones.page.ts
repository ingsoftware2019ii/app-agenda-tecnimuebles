import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ActividadService } from 'src/providers/services/actividad.service';
import { NotificacionesModalPage } from '../notificaciones-modal/notificaciones-modal.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.page.html',
  styleUrls: ['./notificaciones.page.scss'],
})
export class NotificacionesPage implements OnInit {

  constructor(
    private router:Router,
    private modalController: ModalController,
    private _actividadService :ActividadService
  ) { }

  loading = false;
  listaNotificaciones = [/*
    {
      titulo: "Pago a la Sunat",
      descripcion: "Debo realizar un pago urgente",
      fecha_notificacion: "2019-10-12",
      hora_inicio: "08:00:00",
      hora_fin: "09:00:00",
      todo_el_dia: 0,
      estado: 1
    },
    {
      titulo: "Envio de muebles",
      descripcion: "Debo realizar una entrega",
      fecha_notificacion: "2019-10-10",
      hora_inicio: "11:00:00",
      hora_fin: "12:00:00",
      todo_el_dia: 0,
      estado: 1
    }*/
  ];

  ngOnInit() {
    this.listarNotificaciones();
  }

  goBack(){
    this.router.navigate(["/home"]);
  }

  listarNotificaciones(){
    this.loading = true;
    this._actividadService.listarUltimasActividades().subscribe(
      data=>{
        if(data.code === 200){
          this.listaNotificaciones = data.actividad;
          this.loading = false;
        }
      },
      error=>{
        console.log(error);
      }
    )
  }

  async verNotificacion(notificacion) {
    const modal = await this.modalController.create({
      component: NotificacionesModalPage,
      componentProps: {
        'titulo': notificacion.titulo,
        'descripcion': notificacion.descripcion, 
        'fecha_notificacion': notificacion.fecha_notificacion,
        'hora_inicio': notificacion.hora_inicio,
        'hora_fin': notificacion.hora_fin,
        'todo_el_dia': notificacion.todo_el_dia,
        'estado': notificacion.estado
      }
    });
    return await modal.present();
  }
}
