import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'agenda', loadChildren: './agenda/agenda.module#AgendaPageModule' },
  { path: 'contratos', loadChildren: './contratos/contratos.module#ContratosPageModule' },
  { path: 'clientes', loadChildren: './clientes/clientes.module#ClientesPageModule' },
  { path: 'perfil', loadChildren: './perfil/perfil.module#PerfilPageModule' },
  { path: 'cambio-password', loadChildren: './cambio-password/cambio-password.module#CambioPasswordPageModule' },
  { path: 'notificaciones', loadChildren: './notificaciones/notificaciones.module#NotificacionesPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'cliente-modal', loadChildren: './cliente-modal/cliente-modal.module#ClienteModalPageModule' },
  { path: 'contrato-modal', loadChildren: './contrato-modal/contrato-modal.module#ContratoModalPageModule' },
  { path: 'notificaciones-modal', loadChildren: './notificaciones-modal/notificaciones-modal.module#NotificacionesModalPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
