import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteModalPage } from './cliente-modal.page';

describe('ClienteModalPage', () => {
  let component: ClienteModalPage;
  let fixture: ComponentFixture<ClienteModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
