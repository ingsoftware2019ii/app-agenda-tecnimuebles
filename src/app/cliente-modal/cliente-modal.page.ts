import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-cliente-modal',
  templateUrl: './cliente-modal.page.html',
  styleUrls: ['./cliente-modal.page.scss'],
})
export class ClienteModalPage {

  constructor(
    navParams: NavParams,
    private modalCtrl: ModalController
  ) {
    this.cliente.id_cliente = navParams.get('id_cliente');
    this.cliente.nombre = navParams.get('nombre');
    this.cliente.apellidos = navParams.get('apellidos');
    this.cliente.telefono = navParams.get('telefono');
    this.cliente.direccion = navParams.get('direccion');
    this.cliente.correo = navParams.get('correo');
   }

  cliente = {
    id_cliente : 0,
    nombre: "",
    apellidos: "",
    telefono: "",
    direccion: "",
    correo: ""
  }

  dismissModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
