import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {

  constructor(
    private router:Router,
  ) { }

  ngOnInit() {
  }

  usuario={
    apemat: "Mancilla",
    apepat: "Bulleje",
    bloqueo: 0,
    correo: "emiliano-mancilla@gmail.com",
    dni: "70918275",
    id_usuario: 1,
    motivo_bloqueo: "",
    nombre: "Emiliano",
    telefono: "958499365"
  };

  goBack(){
    this.router.navigate(["/home"]);
  }
}
