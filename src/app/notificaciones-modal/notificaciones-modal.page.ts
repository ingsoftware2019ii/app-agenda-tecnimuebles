import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-notificaciones-modal',
  templateUrl: './notificaciones-modal.page.html',
  styleUrls: ['./notificaciones-modal.page.scss'],
})
export class NotificacionesModalPage {

  constructor(
    navParams: NavParams,
    private modalCtrl: ModalController
  ) {
    this.notificacion.titulo = navParams.get('titulo');
    this.notificacion.descripcion = navParams.get('descripcion');
    this.notificacion.fecha_notificacion = navParams.get('fecha_notificacion');
    this.notificacion.hora_inicio = navParams.get('hora_inicio');
    this.notificacion.hora_fin = navParams.get('hora_fin');
    this.notificacion.todo_el_dia = navParams.get('todo_el_dia');
    this.notificacion.estado = navParams.get('estado');
   }

  notificacion = {
    titulo: "",
    descripcion: "",
    fecha_notificacion: "",
    hora_inicio: "",
    hora_fin: "",
    todo_el_dia: 0,
    estado: 1
  }

  dismissModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
