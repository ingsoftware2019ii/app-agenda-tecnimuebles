import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClienteService } from 'src/providers/services/cliente.service';
import { ModalController } from '@ionic/angular';
import { ClienteModalPage } from '../cliente-modal/cliente-modal.page';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {

  constructor(
    private router:Router,
    private _clienteService: ClienteService,
    private modalController: ModalController
  ) { }
  loading = false;
  listaClientes = [/*
    {
      id_cliente: 1,
      nombres: "Luis Antony",
      apellidos: "Cáceres Hinostroza",
      telefono: "958499365",
      direccion: "Sector 1 Grupo 17 Manzana k Lote 10 Villa El Salvador",
      correo: "luis_antony_123_1999@gmail.com"
    },
    {
      id_cliente: 2,
      nombres: "Luis Antony2",
      apellidos: "Cáceres Hinostroza",
      telefono: "958499365",
      direccion: "Sector 1 Grupo 17 Manzana k Lote 10 Villa El Salvador",
      correo: "luis_antony_123_1999@gmail.com"
    }*/
  ];
  listaClientesAux = [];

  cliente = {
    id_cliente : 0,
    nombre: "",
    apellidos: "",
    telefono: "",
    direccion: "",
    correo: ""
  }

  ngOnInit() {
    this.listarClientes();
  }

  ngOnDestroy(){
    this.loading = false;
  }

  goBack(){
    this.router.navigate(["/home"]);
  }

  listarClientes(){
    this.loading = true;
    this._clienteService.listarClientes(0).subscribe(
      data=>{
        console.log(data);
        if(data.code === 200){
          this.listaClientes=data.cliente;
          this.listaClientesAux = data.cliente;
          this.loading = false;
        }
      },
      error=>{
        console.log(<any>error);
      }
    
    )
  }

  async verCliente(cliente) {
    const modal = await this.modalController.create({
      component: ClienteModalPage,
      componentProps: {
        'id_cliente' : cliente.id_cliente,
        'nombre': cliente.nombre,
        'apellidos':  cliente.apellidos,
        'telefono': cliente.telefono,
        'direccion': cliente.direccion,
        'correo': cliente.correo
      }
    });
    return await modal.present();
  }

  search(term: string) {
    if(!term) {
      this.listaClientes = this.listaClientesAux;
    } else {
      this.listaClientes = this.listaClientesAux.filter(x => 
         x.nombre.trim().toLowerCase().includes(term.trim().toLowerCase()) ||
         x.apellidos.trim().toLowerCase().includes(term.trim().toLowerCase()) ||
         x.telefono.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }
}


