import { Component, NgZone, ViewChild, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { MenuController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { formatDate } from '@angular/common';
import { ActividadService } from 'src/providers/services/actividad.service';
import { NotificacionesService } from 'src/providers/services/notificaciones.service';
 
//import { Socket } from 'ngx-socket-io';
//import { GLOBAL } from '../../services/global';
//import { Insomnia } from '@ionic-native/insomnia/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})

export class HomePage {
  constructor(
    private menu: MenuController,
    private router:Router,
    //private socket:Socket,
    //private insomnia:Insomnia,
    private _ngZone:NgZone,
    private alertController:AlertController,
    @Inject(LOCALE_ID) private locale: string,
    private _actividadService: ActividadService,
    private _notificacionesService: NotificacionesService
  ) {}


  /*
  async ionViewWillEnter(){
    this.flag_timer=true;
    this.disableStartButton();
    this.socket.connect();
    clearInterval(this.timer_interval);
    this.verificarViajesEnCurso();
    this.socketCambioAsientos();
    this.insomnia.keepAwake().then(
      ()=>{
      },
      ()=>{
      }
    )
  }*/

  /*
  ionViewWillLeave(){
    this.socket.disconnect();
    this.insomnia.allowSleepAgain().then(
      ()=>{
      },
      ()=>{
      }
    );
    this.menu.close('menu');
  }*/

  openMenu(){
    this.menu.enable(true, 'menu');
    this.menu.open('menu');
  }

  closeMenu(){
    this.menu.enable(false, 'menu');
    this.menu.close('menu');
  }

  goPerfil(){
    this.router.navigate(["/perfil"])
  }

  goNotificaciones(){
    this.router.navigate(["/notificaciones"]);
  }

  goCambioPassword(){
    this.router.navigate(["/cambio-password"])
  }

  goClientes(){
    this.router.navigate(['/clientes']);
  }

  goContratos(){
    this.router.navigate(['/contratos']);
  }



  closeSession(){
    /*GLOBAL.appToken=null;
    this._usuarioService.registrarTokenNotification(localStorage.getItem('token')).subscribe(
      data=>{
        localStorage.clear();
        this.router.navigate(["/login"]);
      },
      error=>{
        console.log(<any>error);
      }
    );*/
    this.presentAlert("¿Está seguro que desea salir?");
  }

  //Home buttons
  enableStartButton(){
    document.getElementById('enabled').style.display="inline";
    document.getElementById('disabled').style.display="none";
  }

  disableStartButton(){
    document.getElementById('enabled').style.display="none";
    document.getElementById('disabled').style.display="inline";
  }

  fixInputs(data){
    var hora_start=data.hora_inicio.slice(0,-3);
    data.hora_start=hora_start;
  }

  /*socketCambioAsientos(){
    var id_conductor=JSON.parse(localStorage.getItem('usuario')).id_conductor;
    this.socket.on('c.listen.update_asientos_itinerario_conductor_id_'+id_conductor,
      data=>{
        clearInterval(this.timer_interval);
        this.verificarProximoViaje();
      })
  }*/

  async presentAlert(mensaje) {
    const alert = await this.alertController.create({
       message: mensaje,
       buttons: [
        {
          text: 'SÍ',
          handler: () => {
            navigator['app'].exitApp();
          }
        },
        {
          text: 'NO',
          handler:()=>{
          }
        }
       ]
     });
    await alert.present();
  }




  event = {
    id_actividad : 0,
    title: '',
    desc: '',
    startTime: '',
    endTime: '',
    allDay: false
  };
 
  minDate = new Date().toISOString();
 
  eventSource = [];
  viewTitle;
 
  calendar = {
    mode: 'month',
    currentDate: new Date(),
  };
 
  @ViewChild(CalendarComponent, {static: false}) myCal: CalendarComponent;
 

 
  ngOnInit() {
    this.obtenerActividades();
    this.resetEvent();
  }
 
  resetEvent() {
    this.event = {
      id_actividad: 0,
      title: '',
      desc: '',
      startTime: new Date().toISOString(),
      endTime: new Date().toISOString(),
      allDay: false
    };
  }
 
  // Create the right event format and reload source
  addEvent() {
    let eventCopy = {
      id_actividad: this.event.id_actividad,
      title: this.event.title,
      startTime:  new Date(this.event.startTime),
      endTime: new Date(this.event.endTime),
      allDay: this.event.allDay,
      desc: this.event.desc
    }
 
    if (eventCopy.allDay) {
      let start = eventCopy.startTime;
      let end = eventCopy.endTime;
 
      eventCopy.startTime = new Date(Date.UTC(start.getUTCFullYear(), start.getUTCMonth(), start.getUTCDate()));
      eventCopy.endTime = new Date(Date.UTC(end.getUTCFullYear(), end.getUTCMonth(), end.getUTCDate() + 1));
    }
    this.enviarNofificacion(eventCopy);
    this.registrarActividad(eventCopy);
    this.eventSource.push(eventCopy);
    this.myCal.loadEvents();
    this.resetEvent();
  }

  // Change current month/week/day
  next() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slideNext();
  }
 
  back() {
    var swiper = document.querySelector('.swiper-container')['swiper'];
    swiper.slidePrev();
  }
  
  // Change between month/week/day
  changeMode(mode) {
    this.calendar.mode = mode;
  }
  
  // Focus today
  today() {
    this.calendar.currentDate = new Date();
  }
  
  // Selected date reange and hence title changed
  onViewTitleChanged(title) {
    let ingles = ['January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December']

    let espanol = [
      'Enero',
      'Febrero',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Agosto',
      'Setiembre',
      'Octubre',
      'Noviembre',
      'Diciembre'
    ]

    ingles.forEach((e, index) => {
      if(e == title.substring(0, title.length-5)){
        this.viewTitle = espanol[index] + title.substring(title.length-5, title.length)
      }
    });
  }
  
  // Calendar event was clicked
  async onEventSelected(event) {
    // Use Angular date pipe for conversion
    let start = formatDate(event.startTime, 'medium', this.locale);
    let end = formatDate(event.endTime, 'medium', this.locale);
  
    const alert = await this.alertController.create({
      header: event.title,
      subHeader: event.desc,
      message: 'De: ' + start + '<br>A: ' + end,
      buttons: [
        {
          text: 'Ok',
          handler: () => {
          }
        },
        {
          text: 'Editar',
          handler:()=>{
            this.event = event;
          }
        }
       ]
    });
    alert.present();
  }
  
  // Time slot was clicked
  onTimeSelected(ev) {
    let selected = new Date(ev.selectedTime);
    this.event.startTime = selected.toISOString();
    selected.setHours(selected.getHours() + 1);
    this.event.endTime = (selected.toISOString());
  }
  
  //metodos

  registrarActividad(evento){
    this.loading = true;
    console.log("Inicio de registro")
    console.log(evento);
    const data={
    id_actividad: evento.id_actividad,
      titulo: evento.title,
      descripcion: evento.desc,
      //fecha_notificacion: evento.StartTime.toString().substring(10,17),
      fecha_notificacion: this.convertir(evento.startTime),
      hora_inicio: evento.startTime.toString().substring(17, 24),
      //hora_inicio2: new Date(Date.parse(evento.startTime)),
      hora_fin: evento.endTime.toString().substring(17, 24),
      todo_el_dia: evento.allDay,
      estado: 1
    }
    console.log(data)
    this._actividadService.registrarActividad(data).subscribe(
      data=>{
        console.log(data);
        if(data.code === 200){
          console.log("Actividad Registrada");
          this.ultimo_id++;
          if(evento.id_actividad == 0){
            evento.id_actividad = this.ultimo_id;
            this.eventSource.push(evento);
          } 
          this.enviarNofificacion(evento);
          this.myCal.loadEvents();
          this.obtenerActividades();
          this.resetEvent();
        }
      },
      error=>{
        console.log(error);
      }
    )
  }

  enviarNofificacion(evento){
    console.log(evento);
    const data = {
    id: evento.id_actividad,
      titulo: evento.title,
      descripcion: evento.desc,
    fecha: new Date(Date.parse(evento.startTime) - 3600000)
    }
    console.log(evento.startTime);
    console.log(Date.parse(evento.startTime))
    console.log(new Date(Date.parse(evento.startTime) - 3600000))
    this._notificacionesService.scheduleNotification(data);
  }
  convertir(fecha){
    fecha=fecha.toLocaleDateString().split('/');
    return fecha[2]+'-'+fecha[1]+'-'+fecha[0];
  }

  loading = false;

  ngOnDestroy(){
    this.loading = false;
  }

  obtenerActividades(){
    this.loading = true;
    this._actividadService.listarActividades().subscribe(
      data=>{
        if(data.code === 200){
          
          data.actividad.forEach(e => {
            //console.log(e.fecha_notificacion.substring(0,10));
            const evento = {
              id_actividad: e.id_actividad,
              title: e.titulo,
              desc: e.descripcion,
              startTime: new Date(e.fecha_notificacion.substring(0,10) + " " + e.hora_inicio),
              endTime: new Date(e.fecha_notificacion.substring(0,10) + " " + e.hora_fin),
              allDay: e.todo_el_dia
            }
            if(e.id_actividad > this.ultimo_id) this.ultimo_id = e.id_actividad;
            this.eventSource.push(evento);
            //console.log(evento);
          });
          console.log(this.eventSource)
          console.log(this.ultimo_id)
          this.myCal.loadEvents();
          
        }
        this.loading = false;
      }
     
    )
    
  }
  ultimo_id = 0;
}
