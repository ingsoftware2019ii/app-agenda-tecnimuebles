import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cambio-password',
  templateUrl: './cambio-password.page.html',
  styleUrls: ['./cambio-password.page.scss'],
})
export class CambioPasswordPage implements OnInit {

  constructor(
    private router:Router,
  ) { }

  ngOnInit() {
  }

  goBack(){
    this.router.navigate(["/home"]);
  }
}
