import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ContratoService } from 'src/providers/services/contrato.service';

@Component({
  selector: 'app-contrato-modal',
  templateUrl: './contrato-modal.page.html',
  styleUrls: ['./contrato-modal.page.scss'],
})
export class ContratoModalPage{

  loading = false;
  constructor(
    navParams: NavParams,
    private modalCtrl: ModalController,
    private _contratoService: ContratoService
  ) {
    this.verContrato(navParams.get('id_contrato'));
   }

   ngOnDestroy(){
     this.loading = false;
   }
  /*contrato = {
    id_contrato: 1,
    fecha: "2019-10-16",
    dias_entrega: 3,
    condicion_pago: 20,
    id_cliente : 1,
    nombres: "Luis Antony",
    apellidos: "Cáceres Hinostroza",
    telefono: "958499365",
    direccion: "Sector 1 Grupo 17 Manzana K Lote 10 Villa El Salvador",
    correo: "luis_antony_123_1999@hotmail.com"
  }*/

  contrato = {
    id_contrato: 0,
    fecha: "",
    dias_entrega: 0,
    condicion_pago: 20,
    id_cliente : 0,
    nombres: "",
    apellidos: "",
    telefono: "",
    direccion: "",
    correo: ""
  }


  verContrato(id_contrato){
    this.loading = true;
    this._contratoService.listarContratoss(id_contrato).subscribe(
      data=>{
        console.log(data);
        if(data.code === 200){
          this.contrato = data.contrato[0];
        }
        this.loading = false;
      },
      error=>{
        console.log(<any>error)
      }
    )
  }

  dismissModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
}
