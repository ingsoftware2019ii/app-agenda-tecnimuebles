import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratoModalPage } from './contrato-modal.page';

describe('ContratoModalPage', () => {
  let component: ContratoModalPage;
  let fixture: ComponentFixture<ContratoModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratoModalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratoModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
