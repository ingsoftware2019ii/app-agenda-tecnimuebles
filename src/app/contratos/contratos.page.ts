import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContratoService } from 'src/providers/services/contrato.service';
import { ModalController } from '@ionic/angular';
import { ContratoModalPage } from '../contrato-modal/contrato-modal.page';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.page.html',
  styleUrls: ['./contratos.page.scss'],
})
export class ContratosPage implements OnInit {

  constructor(
    private router:Router,
    private _contratoService: ContratoService,
    private modalController: ModalController
  ) { }
  loading = false;
  listaContratos = [/*
    {
      id_contrato: 1,
      fecha: "2019-10-16",
      nombres: "Luis Antony",
      apellidos: "Cáceres Hinostroza",
    },
    {
      id_contrato: 2,
      fecha: "2019-10-17",
      nombres: "Marco Antonio",
      apellidos: "Vilca Oropeza",
    }*/
  ];

  listaContratosAux = [];

  contrato = {
    id_contrato: 0,
    fecha: "",
    nombres: "",
    apellidos: "",
  }

  ngOnInit() {
    this.listarContratos();
  }
  ngOnDestroy(){
    this.loading = false;
  }
  listarContratos(){
    this.loading = true;
    this._contratoService.listarContratoss(0).subscribe(
      data=>{
        console.log(data);
        if(data.code === 200){
          this.listaContratos = data.contrato;
          this.listaContratosAux = data.contrato;
          this.loading = false;
        }
      },
      error=>{
        console.log(<any>error);
      }
    )
  }

  goBack(){
    this.router.navigate(["/home"]);
  }

  async verContrato(id_contrato) {
    const modal = await this.modalController.create({
      component: ContratoModalPage,
      componentProps: {
        'id_contrato': id_contrato
      }
    });
    return await modal.present();
  }

  search(term: string) {
    if(!term) {
      this.listaContratos = this.listaContratosAux;
    } else {
      this.listaContratos = this.listaContratosAux.filter(x => 
         x.nombre.trim().toLowerCase().includes(term.trim().toLowerCase()) ||
         x.apellidos.trim().toLowerCase().includes(term.trim().toLowerCase()) ||
         x.telefono.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
    }
  }
}
